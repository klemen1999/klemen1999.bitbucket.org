var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}

var $;
var appId = "3c9de722"
var appKey = "e2eb276ffbb8d41f9c7755be73a799d0"
// This function gets the user input and then jQuery interacts with the API and append the results to the food log. 
function najdiHrano(){
    var text = $("#preberiHrano").val();
    var encodedFood = encodeURIComponent(text);
    $("#list").remove();
// Ajax call to API and then appends the returned info to the food log. 
   $.ajax({
        url: `https://trackapi.nutritionix.com/v2/search/instant?query=`+text,
        headers: {
            'x-app-id': appId,
            'x-app-key': appKey,
            "Content-Type": "application/json"
        },
        "type": "GET",
        "dataType": 'json',
        //'processData': false,
        success: function(response) {
            if(response.common.length==0){
                $("#najdenaHrana").html("<span class='hranaObv label " +
                    "label-danger fade-in'>Žal ni nobene hrane s tem opisom!");
            }else{
                $(".hranaObv").remove();

                narediSeznam(response);  
            }
        },
        error: function(err) {
  			$("#najdenaHrana").html("<span class='hranaObv label " +
          "label-danger fade-in'>Napaka " +
          JSON.parse(err.responseText).userMessage + "!");
  		}

    });
 }

function narediSeznam(hrana){
    $("#seznamHrane").append(
    "<select class="+"form-control"+ " id="+"list"+" multiple='multiple'></select>")
    for(var i=0;i<hrana.common.length;i++){
        $("#list").append("<option>"+hrana.common[i].food_name+"</option>");
    }
    $('#list').change(function () {
        var selectedItem = $(this).children(":selected").val();
        dolocenaHrana(selectedItem);
    });
}

function dolocenaHrana(item){
    $.ajax({
        url: `https://trackapi.nutritionix.com/v2/natural/nutrients`,
        headers: {
            'x-app-id': appId,
            'x-app-key': appKey,
            "Content-Type": "application/json"
        },
        "type": "POST",
        "dataType": 'json',
        'processData': false,
        data: JSON.stringify({"query": item}),
        success: function(response) {
            if(response.foods.length==0){
                $("#dolocenaHranaSporocilo").html("<span class='hranaObv label " +
                    "label-danger fade-in'>Prišlo je do napake pri prikazovanju");
            }else{
                $("#dolocenaHranaSporocilo").remove();
                izrisiInfo(response.foods[0]);
            }
        },
        error: function(err) {
  			$("#dolocenaHranaSporocilo").html("<span class='hranaObv label " +
          "label-danger fade-in'>Napaka " +
          JSON.parse(err.responseText).userMessage + "!");
  		}

    });
}

function izrisiInfo(item){
    $(".hranaInfo").remove();
    $("#dolocenaHrana").append(
    "<div class='hranaInfo panel panel-default'>\
    <div class='panel-heading'>\
    <div><b>"+item.food_name+"</b></div>\
    </div><div class='panel-body'><div class='col-lg-8 col-md-8 col-sm-8'>\
    Število kalorij: "+item.nf_calories+" ("+item.serving_qty+" "
    +item.serving_unit+")<br>Vsebnost ogljikovih hidratov: "+
    item.nf_total_carbohydrate+" g<br>Vsebnost proteinov: "+item.nf_protein+" g<br>\
    Vsebnost maščob: "+item.nf_total_fat+" g</div>\
    <div class='col-lg-4 col-md-4 col-sm-4'><img src='"+item.photo.thumb+"' \
    alt='slika hrane'></div></div><div><button type='submit' \
    class='btn btn-primary btn-s col-lg-4 col-md-4 col-sm-4' \
    id='potrdiHrano'>To sem danes že pojedel</button></div></div>");
    
    $("#potrdiHrano").click(function(){
       zePojedel(item);
    });
}
var doSedaj = 0;

function zePojedel(item){
    $(".progress").remove();
    $(".izracun").remove();
    var ehrId = $("#preberiEHRHrana").val();
      if(ehrId==""||ehrId==null){
        $("#hranaEhrSporocilo").html("<span class='obvestilo label " +
          "label-warning fade-in'>Za uporabo te funkcije morate vnesti EhrId</span>");
      }else{
        $.ajax({
        url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
        type: 'GET',
        headers: {
            "Authorization": getAuthorization()
        },
        success: function (data) {
            var party = data.party;
            $.ajax({
        		url: baseUrl + "/view/" + ehrId + "/" + "blood_pressure",
        		type: 'GET',
        		headers: {
              "Authorization": getAuthorization()
            },
            success:function(data){
              if(data.length==0){
                $("#hranaEhrSporocilo").html("<span class='obvestilo label " +
          "label-warning fade-in'>Ni podatkov za to osebo!</span>");
              }else{
                $("#hranaEhrSporocilo").html("<span></span>");
                var kalorije=1000*data[data.length-1].diastolic;
                doSedaj = doSedaj+item.nf_calories;
                var pr = Math.round((doSedaj/kalorije)*100);
                $("#progressBar").append("<div class='progress' style='margin-top:30px;'><div class=\
                'progress-bar progress-bar-striped bg-success' role='progressbar' style='width:'0%' \
                aria-valuenow='0' aria-valuemin='0' aria-valuemax='100'></div></div>")
                $('.progress').attr('aria-valuenow', doSedaj).css('width', pr+"%")
                                .attr('aria-valuemax', kalorije);
                if(pr>100){
                  var presezek = Math.round(doSedaj-kalorije);
                  $("#progressBar").append("<div class='izracun' style='margin-top:30px;'><h3>\
                    Presegli ste dnevni vnos za <b style='color:#f45f42'> "+presezek+"</b> kalorij</h3></div>")
                  $(".progress").css("background-color","#f45f42"); 
                }else{
                  $("#progressBar").append("<div class='izracun' style='margin-top:30px;'><h3>\
                    Danes ste pojedli:<b> "+pr+" %</b> dnevnih kalorij</h3></div>")  
                }
              }
            }
          });
        }
      });
    }
}
