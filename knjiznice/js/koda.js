
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";

var $;var d3;
var ehrJana; var ehrTone; var ehrMiha;
/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */
function generirajPodatke(stPacienta) {
  ehrId = "";

  // TODO: Potrebno implementirati

  return ehrId;
}


function kreirajEHR(){
  $("#kreirajSporocilo").html("");
  var ime= $("#ime").val();
  var priimek = $("#priimek").val();
  var datumRojstva = $("#datumRojstva").val();
  
  var datumRegex = /^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/;
  var valid = datumRegex.test(datumRojstva);
  if(ime==""|| ime==null || priimek==""||priimek==null || valid==false){
    $("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prišlo je do napake pri vnosu podatkov. Poskusite ponovno!</span>");
    }
  else {
    $.ajaxSetup({
    headers: {
        "Authorization": getAuthorization()
      }
    });
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST',
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = {
          firstNames: ime,
          lastNames: priimek,
          dateOfBirth: datumRojstva,
          additionalInfo: {
            "ehrId":ehrId
          }
        };
        $.ajax({
          url: baseUrl + "/demographics/party/",
          type: 'POST',
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if (party.action == 'CREATE') {
              if(ime=="Jana"){ehrJana = ehrId;}
              if(ime=="Tone"){ehrTone = ehrId;}
              if(ime=="Miha"){ehrMiha = ehrId;}
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR " +
                ehrId + ".</span>");
              $("#preberiEHRid").val(ehrId);
            }
          },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka " +
              JSON.parse(err.responseText).userMessage + " !");
          }
        });
      }
		});
	}
}

function preberiObstojeciEHR(){
  var ehrId = $("#preberiEHRid").val();
  if(ehrId=="" || ehrId==null){
    $("#preberiSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prišlo je do napake pri vnosu podatkov. Poskusite ponovno!</span>");
  }
	else {
	  $.ajaxSetup({
      headers: {
        "Authorization": getAuthorization()
      }
    });
		$.ajax({
			url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
			type: 'GET',
    	success: function (data) {
  			var party = data.party;
  			$("#preberiSporocilo").html("<h4><span class='label "+ "label-info'>Osnovni podatki: " + party.firstNames + " " +
          party.lastNames + ", rojen/a " + party.dateOfBirth+"</span></h4>");
  		},
  		error: function(err) {
  			$("#preberiSporocilo").html("<span class='obvestilo label " +
          "label-danger fade-in'>Napaka " +
          JSON.parse(err.responseText).userMessage + "!");
  		}
		});
	}
}

function dodajVnosPodatkov(){
  var ehrId = $("#dodajEHR").val();
  var datum = $("#dodajDatum").val();
  var spol = $('input[name=spolRadio]:checked', '#myForm').val();
  var teza = $("#dodajTelesnaTeza").val();
  var visina = $("#dodajVisino").val();
  var starost = $("#dodajStarost").val();
  var aktivnost = $("#dodajAktivnost").val();
  $("#dodajVnosPodatkovSporočilo").html("");
  $("#optimalnaTeza").html("");
  //preveri če so podatki pravilno vnešeni
  if(ehrId==""||ehrId==null || teza==""||teza==null || visina==""||visina==null ||
  aktivnost==""||aktivnost==null||aktivnost>7 ||aktivnost<0 || starost==""||starost==null){
    $("#dodajVnosPodatkovSporočilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prišlo je do napake pri vnosu podatkov. Poskusite ponovno!</span>");
  }
  else{
    vnosPodatkovEhr(ehrId,datum,spol,teza,visina,starost,aktivnost);
    if(spol==1){
      var idealnaTeza = Math.round(56.2+1.41*((visina-152)/2.54));
    }
    else{
      var idealnaTeza = Math.round(53.1+1.36*((visina-152)/2.54));
    }
    $("#optimalnaTeza").html("<span>Vaša optimalna teža je: <b>"+idealnaTeza+" kg</b></span>");
  }
}

function vnosPodatkovEhr(ehrId,datum,spol,teza,visina,starost,aktivnost){
  var kalorije = izracunKalorij(spol,teza,visina,starost,aktivnost);
    var dnevneKalorije = kalorije[0];
    var podatki = {
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datum,
		    "vital_signs/body_weight/any_event/body_weight": teza,
		    "vital_signs/height_length/any_event/body_height_length": visina,
		    //to uporabljamo za spol
		    "vital_signs/blood_pressure/any_event/systolic": spol,
		    //to uporabljam za dnevneKalorije/1000
		    "vital_signs/blood_pressure/any_event/diastolic": dnevneKalorije/1000,
		    //to uporabljamo za starost
		    "vital_signs/body_mass_index/any_event/body_mass_index": starost
		    
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: "Mr.Bean"
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": getAuthorization()
      },
      success: function (res) {
        $("#dodajVnosPodatkovSporočilo").html(
          "<span class='obvestilo label label-success fade-in'>" +
          res.meta.href + ".</span>");
      },
      error: function(err) {
      	$("#dodajVnosPodatkovSporočilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
}

//izpis rezultatov v obliki tortnega diagrama
function izpisRezultatov(){
  var ehrId = $("#dodajEHR").val();
  var datum = $("#dodajDatum").val();
  var spol = $('input[name=spolRadio]:checked', '#myForm').val();
  var teza = $("#dodajTelesnaTeza").val();
  var visina = $("#dodajVisino").val();
  var starost = $("#dodajStarost").val();
  var aktivnost = $("#dodajAktivnost").val();
  //preveri če so podatki pravilno vnešeni
  if(ehrId==""||ehrId==null || datum==""||datum==null|| teza==""||teza==null || visina==""||visina==null ||
  aktivnost==""||aktivnost==null||aktivnost>7 ||aktivnost<0 || starost==""||starost==null){
    d3.select("#pieChart svg").remove();
    $("#dodajRezultatiSporočilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Najprej izpolni vse potrebne podatke!</span>");
    $("#izracunaneKalorije").html("<span></span>");
  }
  else{
    var kalorije = izracunKalorij(spol,teza,visina,starost,aktivnost);
    $("#dodajRezultatiSporočilo").html("<span></span>");
    $("#izracunaneKalorije").html("<h4><span>Priporočen dnevni vnos: <strong>"+kalorije[0]+
      "</strong> kalorij</span></h4>");
      narisiPieChart(kalorije);
  }
  
}

//nariši pieChart glede na dnevne kalorije
function narisiPieChart(dnevneKalorije){
  d3.select("#pieChart svg").remove();
  var ohOpis = "Hrana, ki vsebuje veliko ogljikovih hidratov je:<br>- kruh<br>"+
  "- testenine<br>- žitarice<br>- riž"
  var pOpis = "Hrana, ki vsebuje veliko proteinov:<br>- meso<br>- ribe<br>"+
  "- jajca<br>- pšenica"
  var mOpis = "Hrana, ki vsebuje veliko maščob:<br>- oreščki<br>- olje"+
  "<br>- sir<br>- jogurt"
  var details = [
  {vrsta:"Ogljikovi hidrati",number:dnevneKalorije[2],opis:ohOpis},
  {vrsta:"Proteini",number:dnevneKalorije[3],opis:pOpis},
  {vrsta:"Maščobe",number:dnevneKalorije[4],opis:mOpis},
  ];
  var r = 150; // outer radius 
  var width=900;var height=385;
  var colors = d3.scaleOrdinal()
      .range(["#7cff7a","#79ffef","#799cff"])
  
  var svg = d3.select("#pieChart").append("svg")
    .attr("width",width).attr("height",height)
  
  var data = d3.pie().sort(null).value(function(d){return d.number;})
  (details);
  
  var tooltip = d3.select('#pieChart')                               
          .append('div')                                              
          .attr('class', 'tooltip');                                  
  tooltip.append('div').attr('class', 'label').style("color","#353535");
  
  var segments = d3.arc()
                  .innerRadius(0)
                  .outerRadius(r)
                  
  var arcOver = d3.arc()
                  .innerRadius(0)
                  .outerRadius(r + 10)
                  
  var sections = svg.append("g").attr("transform","translate(350,175)")
                  .selectAll("path").data(data)
  
  sections.enter().append("path").attr("d",segments)
                  .attr("fill",function(d){return colors(d.data.number);})
                  .attr("stroke", "black")
                  .style("stroke-width", "2px")
                  .on('mouseover', function (d, i) {
                    d3.select(this).transition()
                    .duration('50')
                    .attr('opacity', '.80')
                    .attr('d', arcOver);

                    tooltip.select('.label').html(d.data.opis);
                    tooltip.style('display', 'block');
                    tooltip.style("opacity", "1"); 
                    tooltip.attr("transform","translate(350,175)");
                  })
                  
                  .on('mouseout', function (d, i) {
                    d3.select(this).transition()
                    .duration('50')
                    .attr('opacity', '1')
                    .attr('d', segments)
                    tooltip.style("opacity","0");})
                  
  var content = d3.select("g").selectAll("text").data(data);
  
  content.enter().append("text").classed("krog",true)
  .attr("transform", function(d){ return "translate(" + segments.centroid(d) +")"; })
  .attr("text-anchor", "middle")
  .text(function(d){ return d.data.number+" g"; });

  
  var legends = svg.append("g").attr("transform","translate(565,50)")
                    .selectAll(".legends").data(data);

  var legend = legends.enter().append("g").classed("legends",true)
                .attr("transform",function(d,i){return "translate(0,"+(i+1)*30+")";});
  
  legend.append("rect").attr("width",20).attr("height",20)
            .attr("fill",function(d){return colors(d.data.number);});
  
  legend.append("text").classed("legenda",true).text(function(d){return d.data.vrsta})
              .attr("fill","#070707")
              .attr("x",30).attr("y",17);
  
  var footer = svg.append("text").text("Graf je 'interaktiven'")
                  .attr("transform","translate(295,365)").style('fill', '#9b9b9b');
}

function izracunKalorij(spol,teza,visina,starost,aktivnost){
  var dnevneKalorije;
  if(spol==1){
      dnevneKalorije = 10*teza+6.25*visina-5*starost+5;
      
    }
    else if(spol==2){
      dnevneKalorije = 10*teza+6.25*visina-5*starost-161;
    }
    if(aktivnost<3){
      dnevneKalorije = dnevneKalorije*1.2;
    }
    else if(aktivnost>=3 && aktivnost<6){
      dnevneKalorije = dnevneKalorije*1.35;
    }
    else{
      dnevneKalorije = dnevneKalorije*1.5;
    }
    var nacin = $("#nacinHujsanja :selected").val();
    if(nacin==2){
      dnevneKalorije=dnevneKalorije*0.9;
    }
    else if(nacin==3){
      dnevneKalorije = dnevneKalorije*1.1;
    }
    if(nacin==1){
      var oh = (Math.round(((0.436*dnevneKalorije)/4) * 100) / 100);
      var p = (Math.round(((0.264*dnevneKalorije)/4) * 100) / 100);
      var m = (Math.round(((0.3*dnevneKalorije)/9) * 100) / 100); 
    }else if(nacin==2){
      var oh = (Math.round(((0.40*dnevneKalorije)/4) * 100) / 100);
      var p = (Math.round(((0.35*dnevneKalorije)/4) * 100) / 100);
      var m = (Math.round(((0.25*dnevneKalorije)/9) * 100) / 100);
    }else if(nacin==3){
      var oh = (Math.round(((0.34*dnevneKalorije)/4) * 100) / 100);
      var p = (Math.round(((0.33*dnevneKalorije)/4) * 100) / 100);
      var m = (Math.round(((0.33*dnevneKalorije)/9) * 100) / 100);
    }
    var kalorije = [];
    dnevneKalorije = (Math.round(dnevneKalorije * 100) / 100)
    kalorije[0] = dnevneKalorije; kalorije[1] = nacin;
    kalorije[2]=oh; kalorije[3]=p; kalorije[4]=m;
    return kalorije;
}

//preberi kartoteko teže
function preberiEHRTeza(){
  var ehrId = $("#preberiEHRidTeza").val();
  if(ehrId==""||ehrId==null){
    $("#tezaSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prišlo je do napake pri vnosu podatkov. Poskusite ponovno!</span>");
  }else{
    $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (data) {
        var party = data.party;
        $("#tezaSporocilo").html("<span>Podatki za: <strong>"+party.firstNames+" "+party.lastNames+"</strong></span>");
        $.ajax({
    		url: baseUrl + "/view/" + ehrId + "/" + "weight",
    		type: 'GET',
    		headers: {
          "Authorization": getAuthorization()
        },
        success:function(data){
          if(data.length==0){
            $("#tezaSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Ni podatkov za to osebo!</span>");
          }else{
            narisiLineChart(data);
            $("#tezaSporocilo").html("<span></span>");
          }
        }
      });
    }
  });
  }
}

function narisiLineChart(data){
  d3.select("#lineChart svg").remove();
  var svgWidth = 600, svgHeight = 400;
  var margin = { top: 35, right: 20, bottom: 30, left: 100 };
  var width = svgWidth - margin.left - margin.right;
  var height = svgHeight - margin.top - margin.bottom;
  var svg = d3.select('#lineChart').append("svg")
    .attr("width", svgWidth)
    .attr("height", svgHeight);
  var g = svg.append("g")
    .attr("transform", 
      "translate(" + margin.left + "," + margin.top + ")"
   );
  var x = d3.scaleTime().rangeRound([0, width]);
  var y = d3.scaleLinear().rangeRound([height, 0]);
  var line = d3.line()
    .x(function(d) { return x(Date.parse(d.time))})
    .y(function(d) { return y(d.weight)})
    x.domain(d3.extent(data, function(d) { return Date.parse(d.time) }));
    y.domain([0, d3.max(data, function(d) { return d.weight; })+10]);
  
  g.append("g")
    .attr("transform", "translate(0," + height + ")")
    .call(d3.axisBottom(x))
    .select(".domain")
    .remove();
   
  g.append("g")
    .call(d3.axisLeft(y))
    .append("text")
    .attr("fill", "#000")
    .attr("transform", "rotate(-90)")
    .attr("y", 6)
    .attr("dy", "0.71em")
    .attr("text-anchor", "end")
    .text("Teža [kg]");
    
  g.append("path")
    .datum(data)
    .attr("fill", "none")
    .attr("stroke", "#54ceff")
    .attr("stroke-linejoin", "round") 
    .attr("stroke-linecap", "round")
    .attr("stroke-width", 3)
    .attr("d", line);
}

function generiraj(){
    $('#predlogaGenerirano').removeAttr('disabled');
    $('#preberiObstojeciEHR').removeAttr('disabled');
    $('#preberiVnos').removeAttr('disabled');
    $('#preberiObstojeciVitalniZnak').removeAttr('disabled');
    $('#hranaObstojeci').removeAttr('disabled');
    ehrJana = "771242de-ef62-491e-8b5e-7d3d6e72f9ea";
    ehrTone = "0f8e75e6-29fa-43e0-8521-b4e0035b4988";
    ehrMiha = "14b92f9c-30b5-4790-9086-1181b8e90f9a";
}

//testni primeri
$(document).ready(function(){
    $('#predlogaGenerirano').change(function() {
      $("#kreirajSporocilo").html("");
      var podatki = $(this).val().split("|");
      $("#ime").val(podatki[0]);
      $("#priimek").val(podatki[1]);
      $("#datumRojstva").val(podatki[2]);
    });
    
    $('#preberiObstojeciEHR').change(function() {
      $("#preberiSporocilo").html("");
      var podatki = $(this).val();
      if(podatki=="ehrJana"){
        $("#preberiEHRid").val(ehrJana);
      }
      if(podatki=="ehrTone"){
        $("#preberiEHRid").val(ehrTone);
      }
      if(podatki=="ehrMiha"){
        $("#preberiEHRid").val(ehrMiha); 
      }else if(podatki==""){
        $("#preberiEHRid").val(""); 
      }
    });
    
    $('#preberiVnos').change(function() {
      $("#dodajVnosPodatkovSporočilo").html("");
      $("#optimalnaTeza").html("");
      var podatki = $(this).val().split("|");
      if(podatki[0]==1){
        $("#dodajEHR").val(ehrJana);
        $("#zenska").prop("checked", true);
        $("#moski").prop("checked", false);
      }
      if(podatki[0]==2){
        $("#dodajEHR").val(ehrTone);
        $("#zenska").prop("checked", false);
        $("#moski").prop("checked", true);
      }
      if(podatki[0]==3){
        $("#dodajEHR").val(ehrMiha);
        $("#zenska").prop("checked", false);
        $("#moski").prop("checked", true);
      }else if(podatki==""){
        $("#dodajEHR").val("");
        $("#zenska").prop("checked", false);
        $("#moski").prop("checked", true);
      }
      $("#dodajDatum").val(podatki[1]);
  		$("#dodajTelesnaTeza").val(podatki[2]);
  		$("#dodajVisino").val(podatki[3]);
  		$("#dodajStarost").val(podatki[4]);
  		$("#dodajAktivnost").val(podatki[5]);
    });
    
    //kreiranje nekaj vnosov
    $('#preberiObstojeciVitalniZnak').change(function() {
      $("#tezaSporocilo").html("");
      var podatki = $(this).val();
      if(podatki==1){
        $("#preberiEHRidTeza").val(ehrJana); 
        vnosPodatkovEhr(ehrJana,"2016-08-13T12:35Z","2","56","166","34","3");
        vnosPodatkovEhr(ehrJana,"2017-04-01T18:16Z","2","52","165","35","4");
        vnosPodatkovEhr(ehrJana,"2018-11-25T16:45Z","2","51","164","36","4");
      }
      if(podatki==2){
        $("#preberiEHRidTeza").val(ehrTone);
        vnosPodatkovEhr(ehrTone,"2016-08-13T12:35Z","1","92","179","60","2");
        vnosPodatkovEhr(ehrTone,"2017-04-01T18:16Z","1","98","180","61","1");
        vnosPodatkovEhr(ehrTone,"2018-11-25T16:45Z","1","101","179","62","1");
      }
      if(podatki==3){
        $("#preberiEHRidTeza").val(ehrMiha);
        vnosPodatkovEhr(ehrMiha,"2016-08-13T12:35Z","1","80","183","20","5");
        vnosPodatkovEhr(ehrMiha,"2017-04-01T18:16Z","1","81","184","21","6");
        vnosPodatkovEhr(ehrMiha,"2018-11-25T16:45Z","1","79","185","22","7");
      }else if(podatki==""){
        $("#preberiEHRidTeza").val("");
      }
    });
    
    $('#hranaObstojeci').change(function() {
      $("#hranaEhrSporocilo").html("");
      var podatki = $(this).val();
      if(podatki==1){
        $("#preberiEHRHrana").val(ehrJana);
      }
      if(podatki==2){
        $("#preberiEHRHrana").val(ehrTone);
      }
      if(podatki==3){
        $("#preberiEHRHrana").val(ehrMiha);
      }else if(podatki==""){
        $("#preberiEHRHrana").val("");
      }
    });
})