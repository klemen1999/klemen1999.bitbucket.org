/* global L, distance */
const SLO_LAT=46.055219;
const SLO_LNG=14.508963;
var mapa;
var lokacija=[];

window.addEventListener("load",function(){
    var mapOptions = {
        center: [SLO_LAT, SLO_LNG],
        zoom: 12
    };
    
    mapa = new L.map("mapa_id",mapOptions);
    
    var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');
    mapa.addLayer(layer);

    izrisiMarkerje();
    izrisiBolnice();
    
    mapa.on('click', function(e) {
    for(var i=0;i<lokacija.length;i++){
        mapa.removeLayer(lokacija[i]);
    }
    lokacija.splice(0);
    var latlng = [];
    latlng[1] = e.latlng.lat;latlng[0] = e.latlng.lng;
    lokacija.push(dodajMarker(latlng,"Vaša lokacija","location"));
    pridobiPodatke(function(bolnice){
        L.geoJson(bolnice, {
	        style: function(feature){
                    if(feature.geometry.type=="LineString"){
                        return preveriOznako(feature.geometry.coordinates[0][1],feature.geometry.coordinates[0][0],
                            e.latlng.lat,e.latlng.lng);
                    }
                    else if(feature.geometry.type=="Polygon"){
                        return preveriOznako(feature.geometry.coordinates[0][0][1],feature.geometry.coordinates[0][0][0],
                            e.latlng.lat,e.latlng.lng);
                    }
	        }, 
	        onEachFeature: function (feature, layer) {
                layer.bindPopup("<div>Naziv: <strong>" + feature.properties.name + "</strong><br>Naslov: "+
                feature.properties["addr:street"]+" "+feature.properties["addr:housenumber"]+", "+
                feature.properties["addr:city"]+"</div>");}
            }).addTo(mapa);
        });
    });
    
});

//izrisi markerje
function izrisiMarkerje(){
    pridobiPodatke(function(bolnice){
        for(var i=0;i<bolnice.length;i++){
            var koordinate = bolnice[i].geometry;
            if(koordinate.type == "Point"){
                dodajMarker(koordinate.coordinates,
                bolnice[i].properties.name,"point");
            }
        }
    });
}

var bolnicaStyle = {
    "color": "#ff3f3f",
    "weight": 2,
    "opacity": 0.65
};

//izrisi bolnice
function izrisiBolnice(){
    pridobiPodatke(function(bolnice){
        L.geoJson(bolnice, {
	        style: bolnicaStyle, 
	        onEachFeature: function (feature, layer) {
                layer.bindPopup("<div>Naziv: <strong>" + feature.properties.name + "</strong><br>Naslov: "+
                feature.properties["addr:street"]+" "+feature.properties["addr:housenumber"]+", "+
                feature.properties["addr:city"]+"</div>");}
        }).addTo(mapa);
    })
}

//pridobi podatke iz json
function pridobiPodatke(callback) {
    var xobj = new XMLHttpRequest();
    xobj.overrideMimeType("application/json");
    xobj.open("GET", "https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json", true);
    xobj.onreadystatechange = function () {
    // rezultat ob uspešno prebrani datoteki
    if (xobj.readyState == 4 && xobj.status == "200") {
        var json = JSON.parse(xobj.responseText);
        // vrnemo rezultat
        callback(json.features);
    }
  };
  xobj.send(null);
}

//dodaj markerje
function dodajMarker(latlng, opis, vrsta) {
  if(vrsta == "point"){
    var ikona = new L.Icon({
        iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
        'marker-icon-2x-red.png',
        shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
        'marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });
    var marker = L.marker([latlng[1], latlng[0]],{icon: ikona}).addTo(mapa);
    // Izpišemo želeno sporočilo v oblaček
    marker.bindPopup("<div>Naziv: " + opis + "</div>");      
  }
  if(vrsta == "location"){
    var ikona = new L.Icon({
        iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
        'marker-icon-2x-blue.png',
        shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' + 
        'marker-shadow.png',
        iconSize: [25, 41],
        iconAnchor: [12, 41],
        popupAnchor: [1, -34],
        shadowSize: [41, 41]
    });
    var marker = L.marker([latlng[1], latlng[0]],{icon: ikona}).addTo(mapa);
    // Izpišemo želeno sporočilo v oblaček
    marker.bindPopup("<div>" + opis + "</div>"); 
    return marker;
  }
}

function preveriOznako(lng, lat,clickLAT,clickLNG) {
    //console.log(distance(lng, lat, clickLAT, clickLNG));
  if (distance(lng, lat, clickLAT, clickLNG) <= 5){ 
    bolnicaStyle = {
    "fillColor": "green",
    "fillOpacity":0.85,
    "color":"green"
    };
    return bolnicaStyle;
    }
  else{
    bolnicaStyle = {
    "fillColor": "blue",
    "fillOpacity":0.85,
    "color":"blue"
    };
    return bolnicaStyle;
    }
}

function distance(lat1,lon1,lat2,lon2) {
	var R = 6371; // km 
	var dLat = (lat2-lat1) * Math.PI / 180;
	var dLon = (lon2-lon1) * Math.PI / 180;
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
		Math.sin(dLon/2) * Math.sin(dLon/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var d = R * c;
	return d;
}

